//
//  errors.hpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#ifndef errors_hpp
#define errors_hpp

#include <string>

using namespace std;

namespace errors {

	[[noreturn]] void handle_error(const string& message);

};

#endif /* errors_hpp */
