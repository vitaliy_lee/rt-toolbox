//
//  mem.hpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#ifndef mem_hpp
#define mem_hpp

#include <memory>

namespace mem {
    template<class T> struct DeleterOf;

    template<class OpenSSLType>
    using UniquePtr = std::unique_ptr<OpenSSLType, DeleterOf<OpenSSLType>>;
}

#endif /* mem_hpp */
