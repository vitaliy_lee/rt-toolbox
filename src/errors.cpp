//
//  errors.cpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#include <openssl/bio.h>
#include <openssl/err.h>

//#include "mem.hpp"
#include "errors.hpp"

//    class StringBIO {
//        string data;
//        mem::UniquePtr<BIO_METHOD> methods_;
//        mem::UniquePtr<BIO> bio;
//    public:
//        StringBIO(StringBIO&&) = delete;
//        StringBIO& operator=(StringBIO&&) = delete;
//
//        explicit StringBIO() {
//            methods_.reset(BIO_meth_new(BIO_TYPE_SOURCE_SINK, "StringBIO"));
//            if (methods_ == nullptr) {
//                throw runtime_error("StringBIO: error in BIO_meth_new");
//            }
//
//            BIO_meth_set_write(methods_.get(), [](BIO* bio, const char *data, int len) -> int {
//                string* str = reinterpret_cast<string*>(BIO_get_data(bio));
//                str->append(data, len);
//                return len;
//            });
//
//            bio.reset(BIO_new(methods_.get()));
//            if (bio == nullptr) {
//                throw runtime_error("StringBIO: error in BIO_new");
//            }
//
//            BIO_set_data(bio.get(), &data);
//            BIO_set_init(bio.get(), 1);
//        }
//
//        BIO* get() { return bio.get(); }
//
//        string get_message() && { return move(data); }
//    };

//    [[noreturn]] void print_errors_and_throw(const char* message) {
//        StringBIO bio;
//        ERR_print_errors(bio.get());
//        throw runtime_error(string(message) + "\n" + move(bio).get_message());
//    }

namespace errors {

    [[noreturn]] void print_errors_and_exit(const char* message) {
        fprintf(stderr, "%s\n", message);
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

	[[noreturn]] void handle_error(const string& message) {
		 printf("Error: %s\n", message.c_str());

		 if(ERR_peek_error()) {
			  printf("OpenSSL error stack:\n");

			  BIO* bio = BIO_new_fp(stdout, BIO_NOCLOSE);
			  ERR_print_errors(bio);
			  BIO_free(bio);
		 }

		exit(EXIT_FAILURE);
	}

};
