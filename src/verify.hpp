//
//  verify_store.hpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#ifndef verify_store_h
#define verify_store_h

#include <filesystem>

#include <openssl/x509.h>

#include "mem.hpp"

namespace fs = std::filesystem;

template<> struct mem::DeleterOf<X509_STORE> {
	void operator()(X509_STORE* store) const {
		X509_STORE_free(store);
	}
};

mem::UniquePtr<X509_STORE> open_trust_store(const fs::path& store_path);

void verify_cert_store(const fs::path& store_path);

int verify_cert(X509_STORE* trust_store, X509* cert);

#endif /* verify_store_h */
