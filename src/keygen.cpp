//
//  keygen.cpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#include <iostream>

#include "common.hpp"
#include "keygen.hpp"

using namespace std;

mem::UniquePtr<EVP_PKEY> generate_key(ENGINE* rtEngine) {
    /*************************************************************************
    * Создание контекста генерации ключевой пары ГОСТ Р 34.10-2012 256 бит   *
    *************************************************************************/
    auto ctx = mem::UniquePtr<EVP_PKEY_CTX>(EVP_PKEY_CTX_new_id(NID_id_GostR3410_2012_256, rtEngine));
    if (ctx == nullptr) {
        throw runtime_error("Не удалось создать экземпляр EVP_PKEY_CTX");
    }

    /*************************************************************************
    * Инициализация контекста                                                *
    *************************************************************************/
    auto result = EVP_PKEY_keygen_init(ctx.get());
    if (result == FAIL) {
        throw runtime_error("Не удалось инициализировать EVP_PKEY_CTX");
    }

    /*************************************************************************
    * Установка набора параметров ключевой пары                              *
    *************************************************************************/
    result = EVP_PKEY_CTX_ctrl_str(ctx.get(), "paramset", "A");
    if (result == FAIL) {
        throw runtime_error("Не удалось установить набор параметров");
    }

    /*************************************************************************
    * Генерация ключевой пары                                                *
    *************************************************************************/
    EVP_PKEY* key_ptr = nullptr;                                        // Описатель ключевой пары

    result = EVP_PKEY_keygen(ctx.get(), &key_ptr);
    if (result == FAIL) {
        throw runtime_error("Не удалось сгенерировать ключ");
    }

    auto key = mem::UniquePtr<EVP_PKEY>(key_ptr);

//    /*************************************************************************
//    * Открытие поточного вывода в файл                                       *
//    *************************************************************************/
//    auto bio = mem::UniquePtr<BIO>(BIO_new_file("private.key.pem", "w"));
//    if (bio == nullptr) {
//        cerr << "Failed to create BIO" << endl;
//    }
//
//    /*************************************************************************
//    * Запись ключа в файл                                                    *
//    *************************************************************************/
//    result = PEM_write_bio_PrivateKey(bio.get(), key.get(), nullptr, nullptr, 0, nullptr, nullptr);
//    if (result == FAIL) {
//        cerr << "Failed to write key file" << endl;
//    }

    cout << "Ключ сгенерирован. NID:\t" << EVP_PKEY_id(key.get()) << endl;
    
    return key;
}
