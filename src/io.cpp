//
//  io.cpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 29.10.2021.
//

#include <iostream>
#include <fstream>
#include <functional>

#include "io.hpp"

#define DO_STRING_JOIN(arg1, arg2) arg1 ## arg2
#define STRING_JOIN(arg1, arg2) DO_STRING_JOIN(arg1, arg2)
#define defer(...) \
	ScopeExit STRING_JOIN(scopeExit, __LINE__) = [__VA_ARGS__]()

class ScopeExit {
public:
	template<typename F>
	ScopeExit(F f)
		: mAction(f)
	{}

	~ScopeExit() {
		try {
			mAction();
		} catch (const exception& e) {
			try {
				cerr << e.what() << endl;
			} catch (...) {
			}
		}
	}

private:
	function<void()> mAction;
};

namespace io {

	vector<uint8_t> read_file(const fs::path& file_path) {
		ifstream file(file_path, ifstream::binary);
		if (!file) {
			throw runtime_error("Невозможно открыть файл: " + file_path.string());
		}

		defer(&file) {
			file.close();
		};

		file.seekg(0, ios::end);
		vector<uint8_t> data(static_cast<size_t>(file.tellg()));

		file.seekg(0, ios::beg);
		file.read(reinterpret_cast<char*>(data.data()), data.size());
		if (!file) {
			throw runtime_error("Невозможно прочитать файл: " + file_path.string());
		}

		return data;
	}

};

//    UniquePtr<BIO> operator|(UniquePtr<BIO> lower, UniquePtr<BIO> upper) {
//        BIO_push(upper.get(), lower.release());
//        return upper;
//    }
