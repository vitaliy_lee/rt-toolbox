//
//  Adder.hpp
//  tests
//
//  Created by user on 27.10.2021.
//

#ifndef Adder_h
#define Adder_h

class Adder{
public:
    int addNumber(int a, int b) {
        return a + b;
    }
};

#endif /* Adder_h */
