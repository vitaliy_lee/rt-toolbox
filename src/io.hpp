//
//  io.h
//  rt-toolbox
//
//  Created by Виталий Левашов on 29.10.2021.
//

#ifndef io_h
#define io_h

#include <filesystem>
#include <vector>

#include <openssl/bio.h>

#include "mem.hpp"

using namespace std;
namespace fs = std::filesystem;

template<> struct mem::DeleterOf<BIO> {
	void operator()(BIO* bio) const {
		BIO_free_all(bio);
	}
};

template<> struct mem::DeleterOf<BIO_METHOD> {
	void operator()(BIO_METHOD* bio) const {
		BIO_meth_free(bio);
	}
};

namespace io {
	
	vector<uint8_t> read_file(const fs::path& file_path);

}

#endif /* io_h */
