//
//  verify_store.cpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#include <chrono>
#include <vector>
#include <iostream>
#include <fstream>
#include <functional>

#include <openssl/bio.h>
#include <openssl/pem.h>

#include "common.hpp"
#include "verify.hpp"
#include "errors.hpp"
#include "io.hpp"

using namespace std;

static int unknownExtensionOid = NID_undef;

template<> struct mem::DeleterOf<X509_STORE_CTX> {
    void operator()(X509_STORE_CTX* ctx) const {
        // println("X509_STORE_CTX::drop()");
        X509_STORE_CTX_free(ctx);
    }
};

template<> struct mem::DeleterOf<X509> {
    void operator()(X509* cert) const {
        // println("X509::drop()");
        X509_free(cert);
    }
};

X509* open_certificate(const fs::path& cert_path) {
    X509* cert = nullptr;

    if (cert_path.extension() == ".pem") {
        auto bio = mem::UniquePtr<BIO>(BIO_new_file(cert_path.c_str(), "r"));
        if (bio == nullptr) {
            errors::handle_error("Не удалось открыть файл");
        }

        cert = PEM_read_bio_X509(bio.get(), nullptr, nullptr, nullptr);
    } else {
        auto cert_data = io::read_file(cert_path);
        const unsigned char* data = cert_data.data();
        long len = static_cast<long>(cert_data.size());
        cert = d2i_X509(nullptr, &data, len);
    }

    if (cert == nullptr) {
        errors::handle_error("Не удалось загрузить сертификат");
    }

    return cert;
}

X509_CRL* open_crl(const fs::path& crl_path) {
    X509_CRL* crl = nullptr;
    
    if (crl_path.extension() == ".pem") {
        auto bio = mem::UniquePtr<BIO>(BIO_new_file(crl_path.c_str(), "r"));
        if (bio == nullptr) {
            errors::handle_error("Не удалось открыть файл");
        }

        crl = PEM_read_bio_X509_CRL(bio.get(), nullptr, nullptr, nullptr);
    } else {
        auto crl_data = io::read_file(crl_path);
        const unsigned char* data = crl_data.data();
        long len = static_cast<long>(crl_data.size());
        crl = d2i_X509_CRL(nullptr, &data, len);
    }
        
    if (crl == nullptr) {
        errors::handle_error("Не удалось загрузить список отозванных сертификатов");
    }
    
    return crl;
}

inline bool is_cert_extension(const string& extension) {
    return extension == ".pem" || extension == ".der" || extension == ".crt" || extension == ".cer";
}

inline bool is_crl_extension(const string& extension) {
    return extension == ".pem" || extension == ".der" || extension == ".crl";
}

mem::UniquePtr<X509_STORE> open_trust_store(const fs::path& store_path) {
    auto store = mem::UniquePtr<X509_STORE>(X509_STORE_new());
    if (store == nullptr) {
        errors::handle_error("Не удалось создать экземпляр X509_STORE");
    }

    auto ca_certs_path = store_path / "cacerts";

    for (auto entry : fs::directory_iterator(ca_certs_path)) {
        auto cert_path = entry.path();
        auto extension = cert_path.extension();

        if (!is_cert_extension(extension)) {
            continue;
        }

        auto ca_cert = open_certificate(cert_path);
        cout << "Загрузка 📄\t" << cert_path << endl;

        auto result =  X509_STORE_add_cert(store.get(), ca_cert);
        if (result == FAIL) {
            errors::handle_error("Не удалось добавить в хранилище сертификат корневого УЦ");
        }
    }

    auto intermediate_certs_path = store_path / "intermediatecerts";

    for (auto entry : fs::directory_iterator(intermediate_certs_path)) {
        auto cert_path = entry.path();
        auto extension = cert_path.extension();

        if (!is_cert_extension(extension)) {
            continue;
        }

        auto inter_cert = open_certificate(cert_path);
        cout << "Загрузка 📄\t" << cert_path << endl;

        auto result =  X509_STORE_add_cert(store.get(), inter_cert);
        if (result == FAIL) {
            errors::handle_error("Не удалось добавить в хранилище сертификат промежуточного УЦ");
        }
    }

    auto crls_path = store_path / "crls";

    for (auto entry : fs::directory_iterator(crls_path)) {
        auto crl_path = entry.path();
        auto extension = crl_path.extension();

        if (!is_crl_extension(extension)) {
            continue;
        }
        
        auto crl = open_crl(crl_path);
        cout << "Загрузка 🚫\t" << crl_path << endl;

        auto result = X509_STORE_add_crl(store.get(), crl);
        if (result == FAIL) {
            errors::handle_error("Не удалось добавить в хранилище список отозванных сертификатов");
        }
    }
    
    return store;
}

int verify_cert(X509_STORE* trust_store, X509* cert) {
    auto ctx = mem::UniquePtr<X509_STORE_CTX>(X509_STORE_CTX_new());
    if (ctx == nullptr) {
        errors::handle_error("Не удалось создать экземпляр X509_STORE_CTX");
    }

    auto result = X509_STORE_CTX_init(ctx.get(), trust_store, cert, nullptr);
    if (result == FAIL) {
        errors::handle_error("Не удалось инициализировать X509_STORE_CTX");
    }

    auto verify_param = X509_VERIFY_PARAM_new();
    if (verify_param == nullptr) {
        errors::handle_error("Не удалось создать экземпляр X509_VERIFY_PARAM");
    }

    auto flags = X509_V_FLAG_CRL_CHECK | X509_V_FLAG_CRL_CHECK_ALL | X509_V_FLAG_X509_STRICT | X509_V_FLAG_EXTENDED_CRL_SUPPORT;
    result = X509_VERIFY_PARAM_set_flags(verify_param, flags);
    if (result == FAIL) {
         errors::handle_error("Не удалось установить флаги X509_VERIFY_PARAM");
    }
    
    X509_VERIFY_PARAM_set_depth(verify_param, 2);

//    auto now = chrono::system_clock::now();
//    auto before = now - chrono::years(1);
//    auto check_time = chrono::system_clock::to_time_t(after);
//    X509_VERIFY_PARAM_set_time(verify_param, check_time);
    
    X509_STORE_CTX_set0_param(ctx.get(), verify_param);

    return X509_verify_cert(ctx.get());
}

static bool ignore_unknown_extension(X509_EXTENSION const* ext) {
    ASN1_OBJECT const* obj = X509_EXTENSION_get_object(
        const_cast<X509_EXTENSION*> (ext)
    );

    int nid = OBJ_obj2nid(obj);

    if (nid == NID_undef) {
        cerr << "Критическое расширение не было проигнорировано" << endl;
    } else if (nid == ::unknownExtensionOid) {
        cerr << "Критическое расширение было проигнорировано" << endl;
    }

    return nid != NID_undef;
}

int verify_callback(int ok, X509_STORE_CTX* ctx) {
    if (X509_STORE_CTX_get_error(ctx) == X509_V_ERR_UNHANDLED_CRITICAL_EXTENSION) {
        cout << "ERR_UNHANDLED_CRITICAL_EXTENSION" << endl;

        bool canWeIgnoreIt = true;

        X509* cert = X509_STORE_CTX_get0_cert(ctx);
        STACK_OF(X509_EXTENSION) const* exts = X509_get0_extensions(cert);

        int const extsSize = sk_X509_EXTENSION_num(exts);

        for (int i = 0; i < extsSize && canWeIgnoreIt; i++) {
            X509_EXTENSION * ext = sk_X509_EXTENSION_value(exts, i);
            if (!X509_EXTENSION_get_critical(ext)) {
                continue;
            }

            canWeIgnoreIt = ignore_unknown_extension(ext);
        }

        if ((ok = canWeIgnoreIt ? 1 : 0)) {
            X509_STORE_CTX_set_error(ctx, X509_V_OK);
        }
    } else if (!ok) {
        auto error = X509_STORE_CTX_get_error(ctx);
        auto message = X509_verify_cert_error_string(error);
        cerr << "При проверке произошла ошибка:\t" << message << endl;
    }

    return ok;
}

void verify_cert_store(const fs::path& store_path) {
    cout << "Проверяется 📁\t" << store_path << endl;

    auto trust_store = mem::UniquePtr<X509_STORE>(open_trust_store(store_path));
//    X509_STORE_set_verify_cb(trust_store.get(), verify_callback);

    auto certs_path = store_path / "certs";

    for (auto entry : filesystem::directory_iterator(certs_path)) {
        auto cert_path = entry.path();
        
        if (!is_cert_extension(cert_path.extension())) {
            continue;
        }
        
        auto certificate = mem::UniquePtr<X509>(open_certificate(cert_path));
        auto result = verify_cert(trust_store.get(), certificate.get());
        
        cout << "Проверен 📄\t" << cert_path;

        if (result == SUCCESS) {
            cout << "\t✅" << endl;
        } else {
            cout << "\t❌" << endl;
        }
    }
}
