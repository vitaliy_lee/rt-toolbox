//
//  engine.cpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//
//#include <stdexcept>

#include "common.hpp"
#include "engine.hpp"

using namespace std;

mem::UniquePtr<ENGINE> init_engine() {
    // Создание rtengine и регистрация его в OpenSSL
    auto result = rt_eng_load_engine();
    if (result == FAIL) {
        throw runtime_error("Не удалось загрузить движок");
    }

    // Получение rtengine
    auto engine = mem::UniquePtr<ENGINE>(rt_eng_get0_engine());
    if (engine == nullptr) {
        throw runtime_error("Не удалось получить экземпляр движка");
    }

    // Установка rtengine реализацией по умолчанию
    result = ENGINE_set_default(engine.get(), ENGINE_METHOD_ALL - ENGINE_METHOD_RAND);
    if (result == FAIL) {
        throw runtime_error("Не удалось установить флаги движка");
    }

    return engine;
}
