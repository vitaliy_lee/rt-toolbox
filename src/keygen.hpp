//
//  keygen.hpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#ifndef keygen_hpp
#define keygen_hpp

#include <openssl/engine.h>
#include <openssl/evp.h>

#include "mem.hpp"

template<> struct mem::DeleterOf<EVP_PKEY> {
        void operator()(EVP_PKEY* pkey) const {
            EVP_PKEY_free(pkey);
        }
    };

template<> struct mem::DeleterOf<EVP_PKEY_CTX> {
        void operator()(EVP_PKEY_CTX* ctx) const {
            EVP_PKEY_CTX_free(ctx);
        }
};

mem::UniquePtr<EVP_PKEY> generate_key(ENGINE* rtEngine);

#endif /* keygen_hpp */
