//
//  engine.hpp
//  rt-toolbox
//
//  Created by Виталий Левашов on 28.10.2021.
//

#ifndef engine_hpp
#define engine_hpp

#include <iostream>
#include <memory>

#include <openssl/engine.h>

#include <rtengine/engine.h>

#include "common.hpp"
#include "mem.hpp"
//#include "errors.hpp"

template<> struct mem::DeleterOf<ENGINE> {
    void operator()(ENGINE* engine) const {
        // println("ENGINE::drop()");

        ENGINE_unregister_pkey_asn1_meths(engine);
        ENGINE_unregister_pkey_meths(engine);
        ENGINE_unregister_digests(engine);
        ENGINE_unregister_ciphers(engine);

//        ENGINE_finish(engine);
//        ENGINE_free(engine);

        auto result = rt_eng_unload_engine();
        if (result == FAIL) {
//            errors::handle_error("Не удалось выгрузить движок");
            std::cerr << "Не удалось выгрузить движок" << std::endl;
        }
    }
};

mem::UniquePtr<ENGINE> init_engine();

#endif /* engine_hpp */
