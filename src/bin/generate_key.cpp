//
//  main.cpp
//  rt-keygen
//
//  Created by Виталий Левашов on 22.10.2021.
//

//#include <iostream>
//#include <filesystem>
//#include <vector>
//
//#include "engine.hpp"
//#include "keygen.hpp"
//
//using namespace std;
//namespace fs = std::filesystem;
//
//int main(int argc, const char* argv[]) {
//	if (argc != 2) {
//		cerr << "Необходимо передать путь к ключу" << endl;
//		return EXIT_FAILURE;
//	}
//
//	fs::path key_path = argv[1];
//
//	mem::UniquePtr<ENGINE> engine = init_engine();
//	if (engine == nullptr) {
//		cerr << "Не удалось инициализировать движок" << endl;
//		return EXIT_FAILURE;
//	}
//
//    generate_key(engine.get());
//
//	return EXIT_SUCCESS;
//}
