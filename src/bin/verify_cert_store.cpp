//
//  main.cpp
//  rt-keygen
//
//  Created by Виталий Левашов on 22.10.2021.
//

#include <iostream>
#include <filesystem>
#include <vector>

#include "engine.hpp"
#include "verify.hpp"

using namespace std;
namespace fs = std::filesystem;

int main(int argc, const char* argv[]) {
    if (argc == 1) {
        cerr << "Необходимо передать пути к хранилищам" << endl;
        return EXIT_FAILURE;
    }

    vector<fs::path> store_paths;
    for (int i = 1; i < argc; i++) {
        auto store_path = argv[i];
        store_paths.push_back(move(store_path));
    }

    mem::UniquePtr<ENGINE> engine = init_engine();
    if (engine == nullptr) {
        cerr << "Не удалось инициализировать движок" << endl;
        return EXIT_FAILURE;
    }

    for (auto store_path : store_paths) {
        verify_cert_store(store_path);
    }

//    generate_key(engine.get());

    return EXIT_SUCCESS;
}
