//
//  tests.m
//  tests
//
//  Created by user on 27.10.2021.
//

#import <XCTest/XCTest.h>
#import "Adder.hpp"

//#import "tools.hpp"
//#import <chrono>
#include <iostream>

using namespace std;

@interface tests : XCTestCase

@end

@implementation tests

- (void)setUp {
    cout << "setUp()" << endl;
}

- (void)tearDown {
    cout << "tearDown()" << endl;
}

- (void)testPositiveNumberAddition {
    Adder adder;
    XCTAssertEqual(5, adder.addNumber(2, 3));
}

- (void)testNegativeNumberAddition {
    Adder adder;
    XCTAssertEqual(-5, adder.addNumber(-2, -3));
}

//- (void)testChrono {
//    constexpr auto year = 31556952ll; // seconds in average Gregorian year
//
//    using shakes = std::chrono::duration<int, std::ratio<1, 100000000>>;
//     using jiffies = std::chrono::duration<int, std::centi>;
//     using microfortnights = std::chrono::duration<float, std::ratio<14*24*60*60, 1000000>>;
//     using nanocenturies = std::chrono::duration<float, std::ratio<100*year, 1000000000>>;
//
//     std::chrono::seconds sec(1);
//
//     std::cout << "1 second is:\n";
//
//     // integer scale conversion with no precision loss: no cast
//     std::cout << std::chrono::microseconds(sec).count() << " microseconds\n"
//               << shakes(sec).count() << " shakes\n"
//               << jiffies(sec).count() << " jiffies\n";
//
//     // integer scale conversion with precision loss: requires a cast
//     std::cout << std::chrono::duration_cast<std::chrono::minutes>(sec).count()
//               << " minutes\n";
//
//     // floating-point scale conversion: no cast
//     std::cout << microfortnights(sec).count() << " microfortnights\n"
//               << nanocenturies(sec).count() << " nanocenturies\n";
//}

//- (void)testSuccess {
//    XCTAssertEqual(1, SUCCESS);
//}

//- (void)testExample {
//    auto crl = open_crl("/Users/user/src/rust/crypto/openssl-rt-engine/testdata/tls/crls/empty.crl.pem");
//
//    XCTAssertNotNil(crl);
//
//    X509_CRL_free(crl);
//}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
